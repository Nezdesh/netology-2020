<?php
$variable = [];


//
if (gettype($variable) === "array" || gettype($variable) === "object" || gettype($variable) === "resource") {
    echo $variable . ' Type is other' . '<br>';
} else {

    echo $variable . ' Type is ' . gettype($variable) . '<br>';
}


////
$a = [
    'bool' => is_bool($variable),
    'float' => is_float($variable),
    'int' => is_int($variable),
    'string' => is_string($variable),
    'null' => is_null($variable)
];
foreach ($a as $key => $value) {
    if ($value === true) {
        $b = $key;
        break;
    }
}
if ($b === null) {
    echo 'Ведено значение типа: ' . 'other' . '<br>';
} else {
    echo $variable . ' Type is ' . $b . '<br>';
}