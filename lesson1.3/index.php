<?php

class  NameStandardizer
{
    private $Name;
    private $Surname;
    private $Otchesto;

    public function __construct(array $dat)
    {
        $this->Name = ucfirst(mb_strtolower($dat['Name']));
        $this->Surname = ucfirst(mb_strtolower($dat['Surname']));
        $this->Otchesto = ucfirst(mb_strtolower($dat['Otchesto']));
    }

    private function fullName()
    {
        return $this->Surname . ' ' . $this->Name . ' ' . $this->Otchesto;
    }

    private function fio()
    {
        preg_match_all('~[А-Я]|[A-Z]~', $this->fullName(), $matches);
        ///почему получился многомерный массив?! не понимаю(((
        return implode($matches[0]) ;
    }

    private function surnameAndInitials()
    {
        preg_match('~([А-я]{2,150}) ([А-Я]).+([А-Я])|([A-z]{2,150}) ([A-Z]).+([A-Z])~', $this->fullName(), $matches);
        $m=[];
        foreach ($matches as $value){
            if(!empty($value)){
                $m[]=$value;
            }
        }
        return $m[1] . ' ' . $m[2] . '.' . $m[3] . '.';
    }

    public function veiw()
    {
        return [
            'fullName' => $this->fullName(),
            'fio' => $this->fio(),
            'surnameAndInitials' => $this->surnameAndInitials()
        ];
    }
}
//$dat = (['Name' => 'alex', 'Surname' => 'sivenkov', 'Otchesto' => 'sergeevich']);




$array = (['Name' => 'Имя', 'Surname' => 'Фамилию', 'Otchesto' => 'Отчество']);
$pattern = '~[А-я]{2,150}|[A-z]{2,150}~';
foreach ($array as $key => $value) {
    $a=null;
    while (!preg_match($pattern, $a, $matches)) {
        fwrite(STDOUT, "Введите $value.".PHP_EOL."Только буквы.".PHP_EOL."Не мение 2-х знаков:".PHP_EOL);
        $a = trim(fgets(STDIN));
        preg_match($pattern, $a, $matches);
        $dat[$key] = $matches[0];
    }

}



$Dz= new NameStandardizer($dat);
extract($Dz->veiw(),EXTR_SKIP);
echo "Результат: ".PHP_EOL."Полное имя: ".$fullName.PHP_EOL."Фамилия и инициалы: ".$fio.PHP_EOL."Аббревиатура: ".$surnameAndInitials.PHP_EOL;