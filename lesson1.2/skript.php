<?php
$a=null;
$b=null;
fwrite(STDOUT,"Введите первое  число".PHP_EOL);
$a=trim(fgets(STDIN));
while (!is_numeric($a)){
    fwrite(STDERR,"Введите, пожалуйста, число".PHP_EOL);
    $a=trim(fgets(STDIN));
}
fwrite(STDOUT,"Введите второе  число не равное нулю".PHP_EOL);
$b=trim(fgets(STDIN));
while (!is_numeric($b) or $b==0){
    fwrite(STDERR,"Введите, пожалуйста, число не равное нулю".PHP_EOL);
    $b=trim(fgets(STDIN));
}
fwrite(STDOUT,"Результат равен= ".$a/$b.PHP_EOL);