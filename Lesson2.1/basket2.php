<?php

// объявление констант и переменных
// объявление созданных функций
const OPERATION_EXIT = 0;
const OPERATION_ADD = 1;
const OPERATION_DELETE = 2;
const OPERATION_PRINT = 3;

$operations = [
    OPERATION_EXIT => OPERATION_EXIT . '. Завершить программу.',
    OPERATION_ADD => OPERATION_ADD . '. Добавить товар в список покупок.',
    OPERATION_DELETE => OPERATION_DELETE . '. Удалить товар из списка покупок.',
    OPERATION_PRINT => OPERATION_PRINT . '. Отобразить список покупок.',
];



function spisokOp(array $operations)
{
    do {
        echo implode(PHP_EOL, $operations) . PHP_EOL . '> ';
        $operationNumber = trim(fgets(STDIN));

        if (!array_key_exists($operationNumber, $operations)) {
            system('clear');

            echo '!!! Неизвестный номер операции, повторите попытку.' . PHP_EOL;
        }
    } while (!array_key_exists($operationNumber, $operations));
    return $operationNumber;
}

function OPERATION_ADD(array $items):array
{
    do {
        echo "Введение название товара для добавления в список: \n> ";
        $itemName = trim(fgets(STDIN));
    } while (empty($itemName));
    $items[] = $itemName;
    return $items;
}

function OPERATION_DELETE(array $items):array
{
    OPERATION_PRINT($items);

do {
    echo 'Введение название товара для удаления из списка:' . PHP_EOL . '> ';
    $itemName = trim(fgets(STDIN));
}while (in_array($itemName, $items)!==true);
 $key = array_search($itemName, $items);
            unset($items[$key]);
    return $items;
}

function OPERATION_PRINT(array $items)
{
    echo 'Ваш список покупок: ' . PHP_EOL;
    echo implode(PHP_EOL, $items) . PHP_EOL;
    echo 'Всего ' . count($items) . ' позиций. ' . PHP_EOL;
    echo 'Нажмите enter для продолжения';
    fgets(STDIN);
}
$items = [];

do {
    $operationNumber = spisokOp($operations);

    echo 'Выбрана операция: ' . $operations[$operationNumber] . PHP_EOL;

    switch ($operationNumber) {
        case OPERATION_ADD:
            $items=OPERATION_ADD($items);
            break;

        case OPERATION_DELETE:
            $items=OPERATION_DELETE($items);
            break;

        case OPERATION_PRINT:
            OPERATION_PRINT($items);
            break;
    }


    echo "\n ----- \n";
} while ($operationNumber > 0);

echo 'Программа завершена' . PHP_EOL;
