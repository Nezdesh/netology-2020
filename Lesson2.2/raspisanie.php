<?php

class Grafik
{
    private $year = 2020;

    private function dif(): int
    {
        $data = $this->year + 1 . '-1-1';
        $datetime1 = new DateTime('now');
        $datetime2 = new DateTime("$data");
        $interval = $datetime1->diff($datetime2);

        return $interval->days;
    }

    private function inf(int $i, string $form = 'w')
    {
        return date("$form", strtotime("+$i days"));
    }

    private function grafikController(): array
    {
        $kalend = [];
        $lastWorkday = -2;
        for ($i = 0; $i <= $this->dif($this->year); $i++) {

            $work = null;
            if ($i - $lastWorkday < 3 ||
                0 == $this->inf($i) ||
                6 == $this->inf($i)) {

            } else {
                $work = 'work';
                $lastWorkday = $i;
            }
            $kalend[$this->inf($i, 'F')][] =
                [
                    //'month' => $this->inf($i, 'F'),
                    'day' => $this->inf($i, 'j'),
                    'dayOfWeek' => $this->inf($i),
                    'work' => $work
                ];


        }
        return $kalend;

    }


    public function contr()
    {
        $k = $this->grafikController();
        echo '<pre>';
        foreach ($k as $key => $day) {
            $this->temple($key, $day);

        }
    }

    private function temple($mon, $day)
    {
        ?>
        <head>
            <title> рабочий график </title>
        </head>
        <body>
        <table width="100%" border="1" cellpadding="4" cellspacing="0">
            <caption>График работы на <?php echo $mon; ?></caption>
            <tr>
                <th>Воскресение</th>
                <th>Понедельник</th>
                <th>Вторник</th>
                <th>Среда</th>
                <th>Четверг</th>
                <th>Пятница</th>
                <th>Суббота</th>

            </tr>

                <?php
                //for($d=0;$d<="count($day)/7";$d++){
                for ($str = 1; $str <= 6; $str++) {
                    ?> <tr> <?php
                    for ($i = 0; $i <= 6; $i++) {

                        $value = array_shift($day);
                        if(!empty($value)){
                        while ($value['dayOfWeek'] != $i && $i<=6) {
                            ?>
                            <th> <?php $i++; ?> </th>
                            <?php
                        }
                        ?>
                        <th><?php echo $value['day'] . ' ' . $value['work']; ?></th>
                        <?php continue;
                    }}
                     ?> </tr> <?php
                } ?>


        </table>
        </body>

        <?php
    }


}

$alex = new Grafik();
$alex->contr();



